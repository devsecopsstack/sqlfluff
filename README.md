# GitLab CI template for SQLFluff

This project implements a GitLab CI/CD template to lint your SQL files (whichever your dialect) with [SQLFluff](https://docs.sqlfluff.com).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/sqlfluff/gitlab-ci-sqlfluff@1.3.1
    # 2: set/override component inputs
    inputs:
      working-dir: "data" # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/sqlfluff'
    ref: '1.3.1'
    file: '/templates/gitlab-ci-sqlfluff.yml'

variables:
  # 2: set/override template variables
  SQLFLUFF_WORKING_DIR: "data" # ⚠ this is only an example
```

## Global configuration

The SQLFluff template uses some global configuration used throughout all jobs.


| Input / Variable | Description                            | Default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `image` / `SQLFLUFF_IMAGE` | The Docker image used to run SQLFluff  | `registry.hub.docker.com/sqlfluff/sqlfluff:latest`   |
| `working-dir` / `SQLFLUFF_WORKING_DIR` | SQLFluff working directory, scope of configuration and sql files used | `.` |

sqlfluff needs to set a [dialect](https://docs.sqlfluff.com/en/stable/dialects.html) you can do it by setting `SQLFLUFF_LINT_ARGS` to `--dialect my_dialect` or provide a [configuration file](https://docs.sqlfluff.com/en/stable/configuration.html) in your repository.

If there is no configuration file in `SQLFLUFF_WORKING_DIR`, [default configuration](https://docs.sqlfluff.com/en/stable/configuration.html#defaultconfig) is used.

## Jobs

### `sqlfluff-lint` job

This job performs a [lint](https://docs.sqlfluff.com/en/stable/cli.html#sqlfluff-lint) analysis of your `SQL` code

It uses the following variable:

| Input / Variable | Description                              | Default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `lint-args` / `SQLFLUFF_LINT_ARGS` | Lint [options and arguments](https://docs.sqlfluff.com/en/stable/cli.html#sqlfluff-lint) | _none_ |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report         | Format                                                                       | Usage             |
| -------------- | ---------------------------------------------------------------------------- | ----------------- |
| `reports/sqlfluff-lint.native.json` | [SQLFluff JSON](https://docs.sqlfluff.com/en/stable/cli.html#cmdoption-sqlfluff-lint-f) format | - |

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).
