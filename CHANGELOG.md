## [1.3.1](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.3.0...1.3.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([1487cab](https://gitlab.com/to-be-continuous/sqlfluff/commit/1487cab1e247cb22b12466ae6fe9c1eff479a5ec))

# [1.3.0](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.2.0...1.3.0) (2024-1-27)


### Features

* migrate to CI/CD component ([7ea798d](https://gitlab.com/to-be-continuous/sqlfluff/commit/7ea798d8e208613b8d20cc8399e88514a202a71a))

# [1.2.0](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.1.2...1.2.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([983df30](https://gitlab.com/to-be-continuous/sqlfluff/commit/983df3079dd8bcaba15204eafda3104d3d9bab72))

## [1.1.2](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.1.1...1.1.2) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([84475be](https://gitlab.com/to-be-continuous/sqlfluff/commit/84475beae2edddb38b0838a3d391daabb0b563bf))

## [1.1.1](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.1.0...1.1.1) (2023-08-01)


### Bug Fixes

* failure while decoding a secret [@url](https://gitlab.com/url)@ does not cause the job to fail (warning message) ([ae3c3f3](https://gitlab.com/to-be-continuous/sqlfluff/commit/ae3c3f32ae530fa46ab888a1ae3c554656954a2c))

# [1.1.0](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.0.2...1.1.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([2a4d5e8](https://gitlab.com/to-be-continuous/sqlfluff/commit/2a4d5e8f0487389d26b8bc7e0959a9f7e311af0c))

## [1.0.2](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.0.1...1.0.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([cc4e4ad](https://gitlab.com/to-be-continuous/sqlfluff/commit/cc4e4ad78ff00fcbb32e8c7fd727d809f07d950a))

## [1.0.1](https://gitlab.com/to-be-continuous/sqlfluff/compare/1.0.0...1.0.1) (2022-08-29)


### Bug Fixes

* exit install ca certs on non root image ([c73cebf](https://gitlab.com/to-be-continuous/sqlfluff/commit/c73cebf890adea2e2b46d487d545da14da4173f4))

# 1.0.0 (2022-08-23)


### Features

* Initialize SQLFluff template ([aaeed4f](https://gitlab.com/to-be-continuous/sqlfluff/commit/aaeed4f992f8d697bbd2b44bf7e85056c610b560))
